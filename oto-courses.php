<?php
/*
Copyright 2015 uClass Developers Daniel Holm & Adam Jacobs Feldstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
if(!empty($_POST)) {
	$fieldCount = count($_POST['pos']);
	for($i=0; $i< $fieldCount; $i++) {
		$post_id= intval(substr($_POST['pos'][$i], strpos($_POST['pos'][$i], ".")+1, strlen($_POST['pos'][$i])));
		$post_pos= intval(substr($_POST['pos'][$i], 4, strpos($_POST['pos'][$i], ":")-4));

		update_post_meta($post_id, 'oto-guide-position', $post_pos);
	}
}

function getCourseCategories() {
	global $wpdb;
	$oto_table_name = $wpdb->prefix . 'oto_settings';

	$slug = $wpdb->get_row("SELECT * FROM `".$oto_table_name."` WHERE `settingsIdentifier` LIKE 'courses_category_slug'");
	//var_dump($slug);
	//echo $slug->settingsValue;
	$idObj = get_category_by_slug($slug->settingsValue); 
	$id = $idObj->term_id;
	$args = array('child_of' => $id);

	return get_categories( $args );
}


function checkGuidePosition() {
	$key_1_value = get_post_meta(get_the_ID(), 'oto-guide-position', true );
	// Check if the custom field has a value.
	//echo $key_1_value;
	if (empty($key_1_value))  {
		update_post_meta(get_the_ID( ), 'oto-guide-position', 0 );
	}
}

// Begin with fething all guides in courses and creating fields for 
// for oto-guide-position if not already exists.'
function doCheckGuidePosition() {
	$categories = getCourseCategories();
	foreach($categories as $category) { 
		global $post; // required
		$args = array('category' => $category->term_id); // include category 9
		$custom_posts = get_posts($args);
		foreach($custom_posts as $post){
			setup_postdata($post);
			checkGuidePosition();
		}
	}
	wp_reset_query();
}

doCheckGuidePosition();
?>
<a class="animated zoomInDown" target="_blank" id="made_by_uclass" href="http://uclass.se/">
	Made by uClassDevs
	<?php
	echo '<img src="' . plugins_url( 'uclass_logo.png', __FILE__ ) . '" > ';
	?>
</a>
<div class="wrap">
	<h1>Courses and Associated Guides </h1>
	<div id="oto-courses-page">
		<?
		$categories = getCourseCategories();
		foreach($categories as $category) { 
			echo '<p><span class="highlighted">Category:</span> <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" '   . '>' . $category->name.'</a> </p> ';
			echo '<p><span class="highlighted">Description:</span> '. $category->description . '</p>';
			echo '<p><span class="highlighted">Course Posts ('.$category->count.'):</span></p>';
		?>
		<ul id="course_<? echo $category->term_id; ?>" class="course-guides">
			<?php global $post; // required
			$args = array('category' => $category->term_id, 'meta_key' => 'oto-guide-position', 'orderby' => 'meta_value_num', 'order'   => 'ASC');
			$custom_posts = get_posts($args);
			foreach($custom_posts as $post) : setup_postdata($post);
			?>

			<li id="guide_<? echo get_the_ID(); ?>"><span class="drag-handle">&#9776;</span> <? the_title();?></li>

			<? 
			endforeach;
			?>
		</ul>
		<script type="text/javascript">
			var phpJsHacks = "<?echo $category->term_id;?>";
			var list = document.getElementById("course_"+phpJsHacks);
			//alert(list);
			Sortable.create(list, {
				animation: 150
			}); 
		</script>
		<?
		}
		?>
		<br>
		<button id="saveSort" class="saveSort button button-primary">Save Sort</button>
		<script type="text/javascript">	
			document.getElementById("saveSort").onclick = function() { 
				var all = document.getElementsByClassName('course-guides');
				var positions = [];

				for(i = 0; i<all.length; i++) {
					for(j = 0; j<all[i].children.length; j++) {
						//console.log(all[i].children[j].id);
						var afterDot = all[i].children[j].id.substr(all[i].children[j].id.indexOf('.'));
						//console.log(afterDot);
						positions.push({pos:"pos_"+j+":id."+afterDot});
					}
				}
				post('', positions);
			};
			//http://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit
			function post(path, params, method) {
				method = method || "post"; // Set method to post by default if not specified.

				// The rest of this code assumes you are not using a library.
				// It can be made less wordy if you use one.
				var form = document.createElement("form");
				form.setAttribute("method", method);
				form.setAttribute("action", path);
				for(i = 0; i < params.length; i++) {
					//console.log(params[i]);
					for(var key in params[i]) {
						//console.log(key);
						if(params[i].hasOwnProperty(key)) {
							//console.log(params[i][key]);
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "pos["+i+"]");
							hiddenField.setAttribute("value", params[i][key]);

							form.appendChild(hiddenField);
						}
					}
				}

				document.body.appendChild(form);
				form.submit();
			}
		</script>
	</div>
</div>