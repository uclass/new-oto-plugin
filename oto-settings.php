<?php 
global $wpdb;
$oto_table_name = $wpdb->prefix . 'oto_settings';

if(!empty($_POST)) {
	$rows_affected = $wpdb->query(
		'UPDATE '. $oto_table_name .' SET settingsValue = \''.sanitize_text_field($_POST['push_api_key']).'\' WHERE id = 1;'
  );
	$rows_affected = $wpdb->query(
		'UPDATE '. $oto_table_name .' SET settingsValue = \''.sanitize_text_field($_POST['courses_category_slug']).'\' WHERE id = 2;'
	);
}

?>
<a class="animated zoomInDown" target="_blank" id="made_by_uclass" href="http://uclass.se/">
	Made by uClassDevs 
	<img src="'.plugins_url( 'uclass_logo.png', __FILE__ ).'" alt="">
</a>
<div class="wrap">
	<form method="post">
		<h1>Settings uClass OTO</h1>
		<table class="form-table">
			<tbody>
				<?php foreach( $wpdb->get_results("SELECT * FROM `".$oto_table_name."`") as $key => $rows): 
				$row= $rows->row;
				$settingsName = $rows->settingsName;
				$settingsIdentifier = $rows->settingsIdentifier;
				$settingsValue = $rows->settingsValue;
				?>
				<tr>
					<th scope="row"><label for="<? echo $settingsIdentifier; ?>"><? echo $settingsName; ?></label></th>
					<?php if($settingsIdentifier == "courses_category_slug"): ?> 
					<td>
						<select name="<? echo $settingsIdentifier; ?>">
							<?php
							$categories = get_categories( array(
								'orderby' => 'name',
								'order'   => 'ASC'
							) );

							foreach( $categories as $category ) {
								$category_name = esc_html( $category->name );
								$category_slug = esc_html( $category->slug );

								if($category_slug == $settingsValue) {
									echo '<option selected value="' . $category_slug . '">'. $category_name .'</option>';
								} else {
									echo '<option value="' . $category_slug . '">'. $category_name .'</option>';
								}
							} 
							?>
						</select>
					</td>
					<? else: ?>
					<td>
						<input name="<? echo $settingsIdentifier; ?>" id="<? echo $settingsIdentifier; ?>" value="<? echo $settingsValue; ?>" class="regular-text" type="text">
					</td>
					<? endif; ?>
				</tr>	
				<?php endforeach; ?>
			</tbody>
		</table>
		<br>
		<input type="submit" class="button button-primary" value="Save Settings">
	</form>
</div>