<?php
/*
Plugin Name: OTO
Description: uClass OTO wordpress plugin for sorting guides in courses and sending push notifications.
Version:     0.1
Author:      Daniel Holm, Adam Jacobs Feldstein
Author URI:  http://uclass.se
License:     Apache License Version 2.0
License URI: http://www.apache.org/licenses/
*/

//For development
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);
@ini_set('display_errors', 1);

function sortableScript( ) {
	wp_enqueue_script( 'sortable_script', plugins_url('new-oto-plugin/js/Sortable.min.js'));
}

add_action('admin_enqueue_scripts', 'sortableScript');

//Load styles for OTO-plugin pages
wp_register_style( 'animate_css',  plugins_url('new-oto-plugin/css/animate.min.css'));
wp_enqueue_style( 'animate_css' );

wp_register_style('uclass_framework', plugins_url('new-oto-plugin/css/uclass-framework.css'));
wp_enqueue_style( 'uclass_framework');

//Create the oto table and columns, also set correct formats
function oto_setup_db() {
	global $wpdb;
	global $oto_db_version;
	$oto_db_version = '0.0.1';

	$oto_table_name = $wpdb->prefix . 'oto_settings';
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $oto_table_name (
    id int(11) NOT NULL AUTO_INCREMENT,
    settingsName text NULL,
    settingsIdentifier text NULL,
   	settingsValue text NULL,
    UNIQUE KEY id (id)
  ) $charset_collate;
  ";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'oto_db_version', $oto_db_version );

	$placeholder_settingsName = "uClass OTO Push Api Key";
	$placeholder_settingsIdentifier = "push_api_key";
	$placeholder_settingsValue = "insert your key";

	$wpdb->insert( $oto_table_name,
								array( 'settingsName' => $placeholder_settingsName, 'settingsIdentifier' => $placeholder_settingsIdentifier, 'settingsValue' => $placeholder_settingsValue ),
								array('%s', '%s')
							 );

	$placeholder_settingsName = "Courses Parent Category";
	$placeholder_settingsIdentifier = "courses_category_slug";
	$placeholder_settingsValue = "courses";

	$wpdb->insert( $oto_table_name,
								array( 'settingsName' => $placeholder_settingsName, 'settingsIdentifier' => $placeholder_settingsIdentifier, 'settingsValue' => $placeholder_settingsValue ),
								array('%s', '%s')
							 );

	//$wpdb->show_errors();
}
register_activation_hook( __FILE__, 'oto_setup_db');

//Setup a widget on dashboard describing css display none classes
function oto_add_dashboard_widgets() {

	wp_add_dashboard_widget(
		'oto_dashboard_widget',         // Widget slug.
		'OTO',         // Title.
		'oto_dashboard_widget_function' // Display function.
	);
}

add_action( 'wp_dashboard_setup', 'oto_add_dashboard_widgets' );
function oto_dashboard_widget_function() {

	// Display whatever you want to tell.
	echo"<h2>The wordpress OTO-plugin</h2>";
	echo "<p>Thank you for using the OTO-plugin. You can learn how to use the plugin here</p>";
}

// Admin Sidebar Menu configuration
add_action('admin_menu', 'addEterMenu');
function addEterMenu() {
	add_menu_page('uClass OTO', 'uClass OTO', 0, 'oto-ios-mobile-options', 'otoMenu');
	add_submenu_page('oto-ios-mobile-options', 'OTO Courses', 'OTO Courses', 0, 'oto-courses', 'otoCourses' );
	add_submenu_page('oto-ios-mobile-options', 'OTO Licences', 'OTO Licences', 0, 'oto-Licences', 'otoLicences' );
}

//Custom push meta box
add_action( 'load-post-new.php', 'oto_push_box_setup' );
add_action( 'load-post.php', 'oto_push_box_setup' );

function oto_push_box_setup() {
	add_action( 'add_meta_boxes', 'oto_add_push_box' );
	add_action( 'save_post', 'oto_handle_push', 10, 2 );
}

function oto_add_push_box() {
	add_meta_box(
		'oto-push-box',      // Unique ID
		esc_html__( 'OTO Management', 'oto' ),    // Title
		'oto_push_box',   // Callback function
		'post',         // Admin page (or post type)
		'side',         // Context
		'high'         // Priority
	);
}

function oto_push_box( $object, $box ) { ?>

<?php wp_nonce_field( basename( __FILE__ ), 'oto_push_box_nonce' ); ?>

<div>
	<b>Post Content:</b>
	<br>
	<input name="oto-update-text" id="oto-update-text" value="1" checked="checked" type="checkbox">
	<label for="oto-update-text">Send to/ publish on OTO</label>
	<br>
	<br>
	<b>Push Notification:</b>
	<br>
	<label for="oto-push-box"><?php _e( "Send a push notification to all your OTO users on post publish/ update.", 'example' ); ?></label>
	<br />
	<b>Push Title:</b>
	<input class="widefat" type="text" name="oto-push-box" id="oto-push-box" value="<?php echo esc_attr( get_post_meta( $object->ID, 'oto_push_box', true ) ); ?>" size="30" />
	<br>
	<b>Push Content:</b>
	<textarea style="width: 100%;" id="oto-push-content" name="oto-push-content" placeholder="Enter push message here."><?php echo esc_attr( get_post_meta( $object->ID, 'oto_push_message', true ) ); ?></textarea>
	<input name="oto-push-send" id="oto-push-send" value="1"  checked="checked" type="checkbox">
	<label for="oto-push-send">Send push notification on Publish or Update</label>
</div>
<?php }

function oto_handle_push( $post_id, $post ) {

	if ( !isset( $_POST['oto_push_box_nonce'] ) || !wp_verify_nonce( $_POST['oto_push_box_nonce'], basename( __FILE__ ) ) ){
		return $post_id;
	}

	$post_type = get_post_type_object( $post->post_type );

	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) ){
		return $post_id;
	}


	$oto_title_value = ( isset( $_POST['oto-push-box'] ) ? sanitize_text_field( $_POST['oto-push-box'] ) : '' );

	$oto_key = 'oto_push_box';

	$meta_value = get_post_meta( $post_id, $oto_key, true );

	if ( $oto_title_value && '' == $meta_value ){
		add_post_meta( $post_id, $oto_key, $oto_title_value, true );
	} elseif ( $oto_title_value && $oto_title_value != $meta_value ){
		update_post_meta( $post_id, $oto_key, $oto_title_value );
	} elseif ( '' == $oto_title_value && $meta_value ){
		delete_post_meta( $post_id, $oto_key, $meta_value );
	}

	$new_meta_value = ( isset( $_POST['oto-push-content'] ) ? sanitize_text_field( $_POST['oto-push-content'] ) : '' );

	$meta_key = 'oto_push_message';

	$meta_value = get_post_meta( $post_id, $meta_key, true );

	if ( $new_meta_value && '' == $meta_value ){
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );
	} elseif ( $new_meta_value && $new_meta_value != $meta_value ){
		update_post_meta( $post_id, $meta_key, $new_meta_value );
	} elseif ( '' == $new_meta_value && $meta_value ){
		delete_post_meta( $post_id, $meta_key, $meta_value );
	}
	if(pushRequestPreCheck()) {
		if(otoPushApiRequest($oto_title_value, $new_meta_value)) {
			//find how to inject to wp admin alert
		} 
	}
}
function getApiKey() {
	global $wpdb;
	$oto_table_name = $wpdb->prefix . 'oto_settings';
	$slug = $wpdb->get_row("SELECT * FROM `".$oto_table_name."` WHERE `settingsIdentifier` LIKE 'push_api_key'");

	return $slug->settingsValue;
}
function pushRequestPreCheck() {
	if (!empty($_POST)) {
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return false;
		}
		if (!isset($_POST['oto_push_box_nonce'])) {
			return false;
		}
		if($_POST['oto-push-send'] == 1 || $_POST['oto-update-text'] == 1) {
			return true;
		}
	}
}

function otoPushApiRequest($title, $message) {
	$apiKey = getApiKey();
	if($_POST['hidden_post_visibility'] == "public" && $_POST['visibility'] == "public") {
		$thePost = array('post_id' => $_POST['post_ID'], 'title' => $_POST['post_title'], 'content' => $_POST['content'] );
	}
	
	if($_POST['oto-push-send'] == 0) {
		$push_notification = array( 'send_push' => $_POST['oto-push-send']);
	} else {
		$push_notification = array( 'send_push' => true, 'push_title' => $title, 'push_message' => $message );
	}

	$data = array( 'api_key' => $apiKey, 'push_notification' => $push_notification, 'post' => $thePost );
	$opts = array('http' =>
								array(
									'method'  => 'POST',
									'header'  => "Content-Type: application/json",
									'content' => json_encode(array('post_request' => $data)),
									'timeout' => 60
								)
							 );
	$context  = stream_context_create($opts);

	$url = 'http://10.0.1.16:3000/api';
	$result = file_get_contents($url, false, $context, -1, 40000);

	$data = json_decode($result , true);

	return $data["success"];
}

function otoMenu() {
	include 'oto-settings.php';
}

function otoCourses() {
	include 'oto-courses.php';
}

function otoLicences() {
	echo'
	<a class="animated zoomInDown" target="_blank" id="made_by_uclass" href="http://uclass.se/">
		Made by uClassDevs
 		<img src="'.plugins_url( 'uclass_logo.png', __FILE__ ).'" alt="">
 	</a>
	<div class="wrap">
		<div id="licenses-wrap">
			<h1>License for WP-OTO</h1>
			<h3>Copyright 2017 uClass Developers Daniel Holm & Adam Jacobs Feldstein</h3>
			<p>
				Licensed under the Apache License, Version 2.0 (the "License");
				you may not use this file except in compliance with the License.
				You may obtain a copy of the License at
			</p>
			<a href="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</a>
			<p>
				Unless required by applicable law or agreed to in writing, software
				distributed under the License is distributed on an "AS IS" BASIS,
				WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
				See the License for the specific language governing permissions and
				limitations under the License.
			</p>
			<h1>Licences for included software</h1>
			<h3>daneden Animate.css</h3>
			<p>Animate.css is licensed under the MIT license. (<a href="http://opensource.org/licenses/MIT">http://opensource.org/licenses/MIT</a>)</p>
			<p>Browse source on github: <href="https://github.com/daneden/animate.css">daneden/animate.css</a></p>
			<h3>Sortable</h3>
			<p>Copyright 2013-2016 Lebedev Konstantin ibnRubaXa@gmail.com <a href="http://rubaxa.github.io/Sortable/">http://rubaxa.github.io/Sortable/</a></p>
			<p>
				Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
			</p>
			<p>
				The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
			</p>
			<p>
				THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
			</p>
		</div>
	</div>
	';
}
?>
